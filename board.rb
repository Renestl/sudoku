require_relative 'tile'

class Board
	attr_reader :grid

	def self.from_file(file)
		puzzle = "puzzles/#{file}"

		rows = File.readlines(puzzle).map(&:chomp)

		tiles = rows.map do |row| 
			nums = row.split("").map(&:to_i)
			nums.map { |num| Tile.new(num) }
		end		

		self.new(tiles)
	end
	
	def self.to_grid
		Array.new(9) {Array.new(9) {Tile.new(0)}}
	end

	def initialize(grid = Board.to_grid)
		@grid = grid
	end

	def update_value(position, new_value)
		row, col = position
		tile = grid[row.to_i][col.to_i]
		tile.value = new_value
	end

	def render
		puts "    #{(0..8).to_a.join(" ")}"
		puts "-----------------------"
		@grid.each_with_index do |row, index|
			puts "#{index} | #{row.map(&:to_s).join(" ")} "

			row_num = index + 1
			if row_num % 3 == 0
				puts "-----------------------"
			end
		end
	end

	def solved?
		rows.all? { |row| solved_section?(row)} && 
			columns.all? { |col| solved_section?(col)} && 
			squares.all? { |square| solved_section?(square)}
	end

	def solved_section?(tiles)
		nums = tiles.map(&:value)
		nums.sort == (1..9).to_a
	end

	def rows
		grid
	end

	def columns
		grid.transpose
	end

	def square(index)
		# define what a square includes
		tiles = []
		rows = (index / 3) * 3
		cols = (index % 3) * 3

		(rows...rows + 3).each do |row|
			(cols...cols + 3).each do |col|
				tiles << grid[row][col]
			end
		end

		tiles
	end

	def squares
		# check all squares
		(0..8).to_a.map { |i| square(i) }
	end

	def [](grid_position)
		row, col = grid_position
		grid[row][col]
	end
end