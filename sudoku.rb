require_relative 'board'

class Sudoku
	def self.from_file(filename)
		board = Board.from_file(filename)
		self.new(board)
	end

	def initialize(board)
		@board = board
	end
	
	def play
		until @board.solved?
			@board.render

			play_pos = get_player_pos
			play_value = get_player_value
			@board.update_value(play_pos, play_value)
		end

		@board.render
		puts "Congratulations, You've Won!!"
	end

	def get_player_pos
		play_pos = nil
		
		until play_pos && valid_pos(play_pos)
			prompt_position
			play_pos = gets.chomp.split(",")
			play_pos.map(&:to_i)
		end

		play_pos
	end

	def get_player_value
		play_val = nil

		until play_val && valid_value(play_val)
			prompt_value
			play_val = gets.chomp
			play_val.to_i
		end

		play_val
	end

	def valid_pos(position)
		position.is_a?(Array) &&
			position.count == 2 &&
			position.all? { |x| x.to_i.between?(0, 8)}
	end

	def valid_value(value)
		value.to_i.is_a?(Integer) &&
		value.to_i.between?(1, 9)
	end

	def prompt_position
		puts "Please enter the position of the tile you would like to change (e.g., '2,3')"
	end

	def prompt_value
		puts "Please enter the new value (e.g., '5')"
	end

end

if __FILE__ == $PROGRAM_NAME
	game = Sudoku.from_file('sudoku1.txt')
	game.play
end