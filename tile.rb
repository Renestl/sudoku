require 'colorize'

class Tile
	attr_reader :value

	def initialize(value)
		@value = value
		@given = value != 0
	end

	def is_given?
		@given
	end

	def color
		is_given? ? :magenta : :cyan
	end

	def to_s
		@given ? value.to_s.colorize(color) : value.to_s.colorize(color)
	end

	def value=(new_value)
		if is_given?
			puts "You cannot change this tile."
		else
			@value = new_value
		end
	end
end